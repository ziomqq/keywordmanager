# Keyword manager

# Introduction

The application gives possibility to define categories and add to them keywords.

### Used technologies
* DOTNET Core v3
* Angular v10
* SQL Server 2019
* Angular Material UI
* Jest

# Getting Started
### Requirements for the development
* DOTNET CORE SDK v3+
* MSSQL optionally in Docker
* Windows, Linux or MacOS
* Some dotnet IDE like VS or Rider and one other IDE like VSC for frontend app development in Angular.

### The application has 2 build units
* WebAPI
* Angular Frontend Application

# Build - using Docker
1. Clone repository
2. Navigate to root folder solution (it should contains file docker-compose.yml)
3. Open CMD in this location
4. Run command:
	
	docker-compose up
	
5. Make a coffee, it will take some time :)
6. I'm sure that during running command some error occur(shame on me :<) - related with sql connection.
   If not, go to step 9
7. Click Ctrl+C or run parallely CMD (in this same location) and run command:

	docker-compose stop
	
8.	After stopping the containers run again:
	
	  docker-compose up
		
	Now it should work :) I hope.. If not, please go to step 7
9. When started navigate to:
	http://localhost:8888
	
10. Enjoy it!
	
# Build - using local resources
1. Clone repository
2. Run the database. Docker image can be used for that:

    docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Qwerty!2313' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest

3. Restore packages

	dotnet restore

4. Build the WebAPI

    dotnet build
	
5. Instal dependencies

	npm i

6. Run angular app

    npm start

7. Start WebAPI

    dotnet run
	
8. When started navigate to:
	http://localhost:4200 

# Test
1. Run frontend tests

    npm test