module.exports = {
    preset: "jest-preset-angular",
    roots: ['<rootDir>/src/'],
    testMatch: ['**/+(*.)+(spec).+(ts)'],
    setupFilesAfterEnv: [
        "<rootDir>/src/test.ts"
    ],
    testPathIgnorePatterns: [
        "<rootDir>/node_modules/",
        "<rootDir>/dist/",
    ],
    testResultsProcessor: "./node_modules/jest-junit-reporter"
};