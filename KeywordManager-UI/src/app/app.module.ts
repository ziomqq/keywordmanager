import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { KeywordManagerHomeComponent } from './keyword-manager-home/keyword-manager-home.component';
import { MaterialModule } from './material/material.module';
import { NewCategoryDialogComponent } from './keyword-manager-home/new-category-dialog/new-category-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GridComponent } from './keyword-manager-home/grid/grid.component';
import { DeleteDialogComponent } from './keyword-manager-home/delete-dialog/delete-dialog.component';
import { KeywordComponent } from './keyword-manager-home/grid/keyword/keyword.component';
import { NewKeywordDialogComponent } from './keyword-manager-home/grid/new-keyword-dialog/new-keyword-dialog.component';
import { environment } from 'src/environments/environment';
import { ApiInterceptor } from './api-interceptor/api-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    TitleBarComponent,
    KeywordManagerHomeComponent,
    NewCategoryDialogComponent,
    GridComponent,
    DeleteDialogComponent,
    KeywordComponent,
    NewKeywordDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: 'BASE_API_URL', useValue: environment.apiUrl },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
