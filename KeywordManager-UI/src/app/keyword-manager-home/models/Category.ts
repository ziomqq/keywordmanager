import { Keyword } from './Keyword';

export class Category {
    id: number | null;
    name: string;
    keywords: Keyword[];

    constructor(name: string) {
        this.name = name;
    }
}
