export class Keyword {
    id: number | null;
    name: string;
    categoryId: number;

    constructor(name: string) {
        this.name = name;
    }
}
