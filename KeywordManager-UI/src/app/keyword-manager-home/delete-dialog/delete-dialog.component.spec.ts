import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { RemovingObject } from '../models/RemovingObject';
import { CategoryService } from '../services/category-service/category.service';
import { KeywordService } from '../services/keyword-service/keyword.service';

import { DeleteDialogComponent } from './delete-dialog.component';

describe('DeleteDialogComponent', () => {
  let component: DeleteDialogComponent;
  let fixture: ComponentFixture<DeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteDialogComponent ],
      imports: [ MatDialogModule, HttpClientTestingModule ],
      providers: [
        CategoryService,
        KeywordService,
        { provide: MatDialogRef, useValue: { close: jest.fn()} },
        HttpClientTestingModule,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        MatSnackBar
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('close is called after cancel dialog', () => {
    const dialogRef = TestBed.inject(MatDialogRef);
    spyOn(dialogRef, 'close');
    component.onNoClick();
    expect(dialogRef.close).toBeCalled();
  });

  it('remove category is called after confirm dialog with category', () => {
    spyOn(component, 'removeCategory');
    component.data.type = RemovingObject.Category;
    component.onRemoveClick();
    expect(component.removeCategory).toBeCalled();
  });

  it('remove keyword is called after confirm dialog with keyword', () => {
    spyOn(component, 'removeKeyword');
    component.data.type = RemovingObject.Keyword;
    component.onRemoveClick();
    expect(component.removeKeyword).toBeCalled();
  });

  it('removeKeyword is called from service when keyword is removing', () => {
    spyOn(component, 'removeKeyword');
    component.data.type = RemovingObject.Keyword;
    component.onRemoveClick();
    expect(component.removeKeyword).toBeCalled();
  });

  it('deleteKeyword is called from service when keyword is removing', () => {
    const service = TestBed.inject(KeywordService);
    spyOn(service, 'deleteKeyword').and.returnValue(of(true));
    component.removeKeyword();
    expect(service.deleteKeyword).toBeCalled();
  });

  it('snackbar is open and dialog is closed after removing keyword', () => {
    const service = TestBed.inject(KeywordService);
    const snackbar = TestBed.inject(MatSnackBar);
    const dialog = TestBed.inject(MatDialogRef);
    spyOn(service, 'deleteKeyword').and.returnValue(of(true));
    spyOn(snackbar, 'open');
    spyOn(dialog, 'close');
    component.removeKeyword();
    expect(snackbar.open).toBeCalled();
    expect(dialog.close).toBeCalled();
  });

  it('deleteCategory is called from service when category is removing', () => {
    const service = TestBed.inject(CategoryService);
    spyOn(service, 'deleteCategory').and.returnValue(of(true));
    component.removeCategory();
    expect(service.deleteCategory).toBeCalled();
  });

  it('snackbar is open and dialog is closed after removing category', () => {
    const service = TestBed.inject(CategoryService);
    const snackbar = TestBed.inject(MatSnackBar);
    const dialog = TestBed.inject(MatDialogRef);
    spyOn(service, 'deleteCategory').and.returnValue(of(true));
    spyOn(snackbar, 'open');
    spyOn(dialog, 'close');
    component.removeCategory();
    expect(snackbar.open).toBeCalled();
    expect(dialog.close).toBeCalled();
  });
});
