import { RemovingObject } from '../models/RemovingObject';

export class DialogData {
    id: number;
    name: string;
    type: RemovingObject;

    constructor(id: number, name: string, type: RemovingObject) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
}
