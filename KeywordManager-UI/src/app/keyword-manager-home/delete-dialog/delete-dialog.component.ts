import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RemovingObject } from '../models/RemovingObject';
import { CategoryService } from '../services/category-service/category.service';
import { KeywordService } from '../services/keyword-service/keyword.service';
import { DialogData } from './dialog-data';

@Component({
  selector: 'delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnDestroy {
  dataType = RemovingObject;
  private unsubscribe$: Subject<void> = new Subject();
  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private categoryService: CategoryService,
    private keywordService: KeywordService,
    private snackBar: MatSnackBar) { }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onRemoveClick(): void {
    switch (this.data.type) {
      case this.dataType.Category:
        this.removeCategory();
        break;
      case this.dataType.Keyword:
        this.removeKeyword();
        break;
    }
  }

  removeCategory(): void {
    this.categoryService.deleteCategory(this.data.id).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.snackBar.open(`Category ${this.data.name}  was deleted`, null, { duration: 2000 });
      this.dialogRef.close(true);
    });
  }

  removeKeyword(): void {
    this.keywordService.deleteKeyword(this.data.id).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.snackBar.open(`Keyword ${this.data.name}  was deleted`, null, { duration: 2000 });
      this.dialogRef.close(true);
    });
  }
}
