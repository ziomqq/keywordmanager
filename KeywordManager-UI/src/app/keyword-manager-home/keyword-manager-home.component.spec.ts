import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';

import { KeywordManagerHomeComponent } from './keyword-manager-home.component';
import { CategoryService } from './services/category-service/category.service';

describe('KeywordManagerHomeComponent', () => {
  let component: KeywordManagerHomeComponent;
  let fixture: ComponentFixture<KeywordManagerHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeywordManagerHomeComponent ],
      imports: [ MatDialogModule, HttpClientTestingModule ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [ CategoryService, HttpClientTestingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordManagerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
