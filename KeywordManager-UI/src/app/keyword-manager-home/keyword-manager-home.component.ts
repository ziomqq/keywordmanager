import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { NewCategoryDialogComponent } from './new-category-dialog/new-category-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { CategoryService } from './services/category-service/category.service';

@Component({
  selector: 'keyword-manager-home',
  templateUrl: './keyword-manager-home.component.html',
  styleUrls: ['./keyword-manager-home.component.scss']
})
export class KeywordManagerHomeComponent implements OnDestroy {

  private unsubscribe$: Subject<void> = new Subject();

  constructor(private dialog: MatDialog, private categoryService: CategoryService) { }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  addNewCateogry(): void {
    const dialogRef = this.dialog.open(NewCategoryDialogComponent);

    dialogRef.afterClosed()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result: boolean) => {
      if (result) {
        this.categoryService.refresh();
      }
    });
  }
}
