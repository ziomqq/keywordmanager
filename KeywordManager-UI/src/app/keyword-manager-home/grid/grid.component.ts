import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { DialogData } from '../delete-dialog/dialog-data';
import { Category } from '../models/Category';
import { RemovingObject } from '../models/RemovingObject';
import { CategoryService } from '../services/category-service/category.service';
import { NewKeywordDialogComponent } from './new-keyword-dialog/new-keyword-dialog.component';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['name', 'keywords', 'action'];
  dataSource = new MatTableDataSource<Category>();
  selection = new SelectionModel<Category>(false, []);
  isVisible = false;
  private unsubscribe$: Subject<void> = new Subject();

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }
  constructor(
    private categoryService: CategoryService,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.categoryService.dataSource().pipe(takeUntil(this.unsubscribe$)).subscribe((shouldRefresh) => {
      if (shouldRefresh) {
        this.getData();
      }
    });
    this.selection.changed.pipe(takeUntil(this.unsubscribe$)).subscribe(row => {
      this.setDeleteButtonVisibility(row);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData(): void {
    this.categoryService.getCategories().pipe(takeUntil(this.unsubscribe$)).subscribe((data: Category[]) => {
      this.dataSource.data = data;
    });
  }

  setDeleteButtonVisibility(data: any): void {
    this.isVisible = data.added.length > 0;
  }

  deleteCateogry(): void {
    const row = this.selection.selected[0];
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data: new DialogData(row.id, row.name, RemovingObject.Category)});
    dialogRef.afterClosed().pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.categoryService.refresh();
      }
    });
  }

  onAddClick(event, selectedId: number): void {
    event.stopPropagation();
    const dialogRef = this.dialog.open(NewKeywordDialogComponent, { data: selectedId });
    dialogRef.afterClosed().pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.categoryService.refresh();
      }
    });
  }
}
