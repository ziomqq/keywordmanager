import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteDialogComponent } from '../../delete-dialog/delete-dialog.component';
import { DialogData } from '../../delete-dialog/dialog-data';
import { Keyword } from '../../models/Keyword';
import { RemovingObject } from '../../models/RemovingObject';
import { CategoryService } from '../../services/category-service/category.service';

@Component({
  selector: 'keyword',
  templateUrl: './keyword.component.html',
  styleUrls: ['./keyword.component.scss']
})
export class KeywordComponent implements OnDestroy {
  @Input() data: Keyword;
  private unsubscribe$: Subject<void> = new Subject();
  constructor(private dialog: MatDialog, private categoryService: CategoryService) { }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onDelete(event): void {
    event.stopPropagation();
    const dialogRef = this.dialog.open(DeleteDialogComponent,
      { data: new DialogData(this.data.id, this.data.name, RemovingObject.Keyword)});

    dialogRef.afterClosed().pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.categoryService.refresh();
      }
    });
  }
}
