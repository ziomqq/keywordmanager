import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { of } from 'rxjs';
import { DeleteDialogComponent } from '../../delete-dialog/delete-dialog.component';
import { Keyword } from '../../models/Keyword';
import { CategoryService } from '../../services/category-service/category.service';

import { KeywordComponent } from './keyword.component';

describe('KeywordComponent', () => {
  let component: KeywordComponent;
  let fixture: ComponentFixture<KeywordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeywordComponent, DeleteDialogComponent ],
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        MatTooltipModule
      ],
      providers: [
        CategoryService,
        HttpClientTestingModule,
        { provide: MatDialogRef, useValue: { afterClosed: jest.fn() } },
        { provide: MatDialog, useValue: { open: jest.fn() }},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('stop propagation is called on delete button click', () => {
    spyOn(TestBed.inject(MatDialog), 'open').and.returnValue({afterClosed: () => of(true) });
    const event = { stopPropagation: jest.fn() };
    component.data = new Keyword('testName');
    component.data.id = 1;
    component.onDelete(event);
    expect(event.stopPropagation).toBeCalled();
  });

  it('dialog is open when delete button is clicked', () => {
    const dialog = TestBed.inject(MatDialog);
    spyOn(dialog, 'open').and.returnValue({afterClosed: () => of(true) });
    const event = { stopPropagation: jest.fn() };
    component.data = new Keyword('testName');
    component.data.id = 1;
    component.onDelete(event);
    expect(dialog.open).toBeCalled();
  });

  it('refresh grid is called when dialog is confirm', () => {
    const dialog = TestBed.inject(MatDialog);
    const service = TestBed.inject(CategoryService);
    spyOn(service, 'refresh');
    spyOn(dialog, 'open').and.returnValue({afterClosed: () => of(true) });
    const event = { stopPropagation: jest.fn() };
    component.data = new Keyword('testName');
    component.data.id = 1;
    component.onDelete(event);
    expect(service.refresh).toBeCalled();
  });

  it('refresh grid is not called when dialog is cancel', () => {
    const dialog = TestBed.inject(MatDialog);
    const service = TestBed.inject(CategoryService);
    spyOn(service, 'refresh');
    spyOn(dialog, 'open').and.returnValue({afterClosed: () => of(false) });
    const event = { stopPropagation: jest.fn() };
    component.data = new Keyword('testName');
    component.data.id = 1;
    component.onDelete(event);
    expect(service.refresh).not.toBeCalled();
  });
});
