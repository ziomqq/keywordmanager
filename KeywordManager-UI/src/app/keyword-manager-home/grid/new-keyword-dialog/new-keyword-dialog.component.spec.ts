import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { KeywordService } from '../../services/keyword-service/keyword.service';

import { NewKeywordDialogComponent } from './new-keyword-dialog.component';

describe('NewKeywordDialogComponent', () => {
  let component: NewKeywordDialogComponent;
  let fixture: ComponentFixture<NewKeywordDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewKeywordDialogComponent ],
      imports: [ MatDialogModule, HttpClientTestingModule ],
      providers: [
        KeywordService,
        HttpClientTestingModule,
        { provide: MatDialogRef, useValue: { close: jest.fn() } },
        { provide: MatDialog, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {}, },
        MatSnackBar
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKeywordDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('close is called after cancel dialog', () => {
    const dialogRef = TestBed.inject(MatDialogRef);
    spyOn(dialogRef, 'close');
    component.onCancelClick();
    expect(dialogRef.close).toBeCalled();
  });

  it('on add click name is validating', () => {
    spyOn(component, 'validateName');
    component.onAddClick();
    expect(component.validateName).toBeCalled();
  });

  it('on add click loading is enabled', (done) => {
    spyOn(component, 'validateName');
    component.onAddClick();
    component.loading$.subscribe(result => {
      expect(result).toBeTruthy();
      done();
    });
  });

  it('snackbar is open and dialog is closed after save keyword', () => {
    const service = TestBed.inject(KeywordService);
    const snackbar = TestBed.inject(MatSnackBar);
    const dialog = TestBed.inject(MatDialogRef);
    spyOn(service, 'addKeyword').and.returnValue(of(true));
    spyOn(snackbar, 'open');
    spyOn(dialog, 'close');
    component.saveKeyword();
    expect(snackbar.open).toBeCalled();
    expect(dialog.close).toBeCalled();
  });

  it('save method is called on confirm dialog', () => {
    const service = TestBed.inject(KeywordService);
    spyOn(service, 'addKeyword').and.returnValue(of(true));
    component.saveKeyword();
    expect(service.addKeyword).toBeCalled();
  });

  it('save method is called when validation is passed', () => {
    const service = TestBed.inject(KeywordService);
    spyOn(service, 'validateName').and.returnValue(of(true));
    spyOn(component, 'saveKeyword');
    component.name = 'new name';
    component.validateName();
    expect(component.saveKeyword).toBeCalled();
    expect(component.isValid).toBeTruthy();
  });

  it('when validation is not passed, flag isValid is false', () => {
    const service = TestBed.inject(KeywordService);
    spyOn(service, 'validateName').and.returnValue(of(false));
    spyOn(component, 'saveKeyword');
    component.name = 'new name';
    component.validateName();
    expect(component.saveKeyword).not.toBeCalled();
    expect(component.isValid).toBeFalsy();
  });

  it('when validation is not passed, form contain error', () => {
    const service = TestBed.inject(KeywordService);
    spyOn(service, 'validateName').and.returnValue(of(false));
    spyOn(component, 'saveKeyword');
    component.name = 'new name';
    component.validateName();
    expect(component.formControl.errors.exists).toBeTruthy();
    expect(component.formControl.status).toEqual('INVALID');
  });
});
