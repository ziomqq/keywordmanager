import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { KeywordService } from '../../services/keyword-service/keyword.service';

@Component({
  selector: 'new-keyword-dialog',
  templateUrl: './new-keyword-dialog.component.html',
  styleUrls: ['./new-keyword-dialog.component.scss']
})
export class NewKeywordDialogComponent implements OnDestroy {

  name = '';
  isValid = true;
  formControl = new FormControl(name, [ Validators.required ]);

  readonly loading$: Observable<boolean>;
  private loadingSource$ = new BehaviorSubject(false);
  private unsubscribe$: Subject<void> = new Subject();

  constructor(
    private dialogRef: MatDialogRef<NewKeywordDialogComponent>,
    private keywordService: KeywordService,
    @Inject(MAT_DIALOG_DATA) public categoryId: number,
    private snackBar: MatSnackBar) {
      this.loading$ = this.loadingSource$.asObservable();
  }

    ngOnDestroy(): void {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }

    onCancelClick(): void {
      this.dialogRef.close();
    }

    onAddClick(): void {
      this.loadingSource$.next(true);
      this.validateName();
    }

    validateName(): void {
      this.keywordService.validateName(this.categoryId, this.formControl.value)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((result: boolean) => {
          if (result) {
            this.isValid = true;
            this.saveKeyword();
          } else {
            this.isValid = false;
            this.formControl.setErrors({ exists: true });
            this.loadingSource$.next(false);
          }
      });
    }

    saveKeyword(): void {
      this.keywordService.addKeyword(this.categoryId, this.formControl.value).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
        this.snackBar.open('New keyword was saved', null, { duration: 2000 });
        this.dialogRef.close(true);
      });
    }
}
