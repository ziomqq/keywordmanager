import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CategoryService } from '../services/category-service/category.service';

import { NewCategoryDialogComponent } from './new-category-dialog.component';

describe('NewCategoryDialogComponent', () => {
  let component: NewCategoryDialogComponent;
  let fixture: ComponentFixture<NewCategoryDialogComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCategoryDialogComponent ],
      imports: [ MatDialogModule, HttpClientTestingModule ],
      providers: [
        CategoryService,
        { provide: MatDialogRef, useValue: {} },
        HttpClientTestingModule,
        MatSnackBar
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCategoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
