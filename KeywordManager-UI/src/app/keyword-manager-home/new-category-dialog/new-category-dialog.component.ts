import { Component, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CategoryService } from '../services/category-service/category.service';

@Component({
  selector: 'new-category-dialog',
  templateUrl: './new-category-dialog.component.html',
  styleUrls: ['./new-category-dialog.component.scss']
})
export class NewCategoryDialogComponent implements OnDestroy {

name = '';
isValid = true;
formControl = new FormControl(name, [ Validators.required ]);

readonly loading$: Observable<boolean>;
private loadingSource$ = new BehaviorSubject(false);
private unsubscribe$: Subject<void> = new Subject();

constructor(
  private dialogRef: MatDialogRef<NewCategoryDialogComponent>,
  private categoryService: CategoryService,
  private snackBar: MatSnackBar) {
  this.loading$ = this.loadingSource$.asObservable();
}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onAddClick(): void {
    this.loadingSource$.next(true);
    this.validateName();
  }

  validateName(): void {
    this.categoryService.validateName(this.formControl.value).pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.isValid = true;
        this.saveCategory();
      } else {
        this.isValid = false;
        this.formControl.setErrors({ exists: true });
        this.loadingSource$.next(false);
      }
    });
  }

  saveCategory(): void {
    this.categoryService.addCategory(this.formControl.value).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.snackBar.open('New category was saved!', null, { duration: 2000 });
      this.dialogRef.close(true);
    });
  }
}
