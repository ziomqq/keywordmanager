import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Category } from '../../models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private refreshData = new BehaviorSubject<boolean>(true);

  constructor(private http: HttpClient) { }

  validateName(name: string): Observable<boolean> {
    return this.http.get<boolean>('category/validate?categoryName=' + name);
  }

  addCategory(name: string): Observable<number> {
    return this.http.post<number>('category', new Category(name));
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>('category');
  }

  refresh(): void {
    this.refreshData.next(true);
  }

  dataSource(): Observable<boolean> {
    return this.refreshData.asObservable();
  }

  deleteCategory(categoryId: number): Observable<number> {
    return this.http.delete<number>('category?categoryId=' + categoryId);
  }
}
