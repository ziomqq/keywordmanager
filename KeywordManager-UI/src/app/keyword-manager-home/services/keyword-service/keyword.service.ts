import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Keyword } from '../../models/Keyword';

@Injectable({
  providedIn: 'root'
})
export class KeywordService {

  constructor(private http: HttpClient) { }

  deleteKeyword(keywordId: number): Observable<number> {
    return this.http.delete<number>('keyword?keywordId=' + keywordId);
  }

  validateName(categoryId: number, name: string): Observable<boolean> {
    return this.http.get<boolean>(`keyword/validate?categoryId=${categoryId}&keywordName=${name}`);
  }

  addKeyword(categoryId: number, name: string): Observable<number> {
    const keyword = new Keyword(name);
    keyword.categoryId = categoryId;
    return this.http.post<number>('keyword', keyword);
  }
}
