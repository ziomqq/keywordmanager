﻿using KeywordManager.Core.Domain.Models;
using KeywordManager.Data.EF;
using System.Linq;
using System.Threading.Tasks;

namespace KeywordManager.Data.Repositories
{
    public class KeywordRepository : IKeywordRepository
    {
        private readonly KeywordManagerContext _context;
        public KeywordRepository(KeywordManagerContext context)
        {
            _context = context;
        }

        public async Task<int> AddOrUpdateKeyword(Keyword keyword)
        {
            Keyword keywordDb = _context.Keywords.Where(x => x.Id == keyword.Id).FirstOrDefault();
            if (keywordDb == null)
            {
                _context.Keywords.Add(keyword);
            }
            else
            {
                _context.Keywords.Update(keyword);
            }

            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteKeyword(int keywordId)
        {
            Keyword keywordDb = _context.Keywords.Where(x => x.Id == keywordId).FirstOrDefault();
            if (keywordDb == null) return 0;

            keywordDb.Deleted = true;

            return await _context.SaveChangesAsync();
        }

        public bool ValidateName(int categoryId, string keywordName)
        {
            Keyword keywordDb = _context.Keywords.Where(x => x.Name == keywordName && x.CategoryId == categoryId).FirstOrDefault();
            return keywordDb == null;
        }
    }
}
