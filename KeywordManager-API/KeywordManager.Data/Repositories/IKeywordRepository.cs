﻿using KeywordManager.Core.Domain.Models;
using System.Threading.Tasks;

namespace KeywordManager.Data.Repositories
{
    public interface IKeywordRepository
    {
        Task<int> AddOrUpdateKeyword(Keyword keyword);
        bool ValidateName(int categoryId, string keywordName);
        public Task<int> DeleteKeyword(int keywordId);
    }
}
