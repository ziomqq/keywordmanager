﻿using KeywordManager.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Data.Repositories
{
    public interface ICategoryRepository
    {
        public Task<List<Category>> GetCategories();
        Task<int> AddOrUpdateCategory(Category category);
        bool ValidateName(string categoryName);
        public Task<int> DeleteCategory(int categoryId);
    }
}
