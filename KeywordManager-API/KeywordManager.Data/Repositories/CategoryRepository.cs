﻿using KeywordManager.Core.Domain.Models;
using KeywordManager.Data.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordManager.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly KeywordManagerContext _context;
        public CategoryRepository(KeywordManagerContext context)
        {
            _context = context;
        }
        public Task<List<Category>> GetCategories()
        {
            return _context.Categories
                .Include(c => c.Keywords)
                .ToListAsync();
        }

        public async Task<int> AddOrUpdateCategory(Category category)
        {
            Category categoryDb = _context.Categories.Where(x => x.Id == category.Id).FirstOrDefault();
            if (categoryDb == null)
            {
                _context.Categories.Add(category);
            }
            else
            {
                _context.Categories.Update(category);
            }

            return await _context.SaveChangesAsync();
        }

        public bool ValidateName(string categoryName)
        {
            Category categoryDb = _context.Categories.Where(x => x.Name == categoryName).FirstOrDefault();
            return categoryDb == null;
        }

        public async Task<int> DeleteCategory(int categoryId)
        {
            Category categoryDb = _context.Categories.Include(c => c.Keywords).Where(x => x.Id == categoryId).FirstOrDefault();
            if (categoryDb == null) return 0;

            categoryDb.Deleted = true;
            foreach(var keyword in categoryDb.Keywords)
            {
                keyword.Deleted = true;
            }

            return await _context.SaveChangesAsync();
        }
    }
}
