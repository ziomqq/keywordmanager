﻿using KeywordManager.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeywordManager.Data.EF.Mappings
{
    public class KeywordMap : IEntityTypeConfiguration<Keyword>
    {
        public void Configure(EntityTypeBuilder<Keyword> builder)
        {
            builder.HasKey(e => e.Id)
                .IsClustered();

            builder.Property(e => e.Created)
                .IsRequired()
                .HasColumnType("DateTime2")
                .HasDefaultValueSql("GETUTCDATE()")
                .HasConversion(v => v,
                    v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            List<Keyword> keywords = new List<Keyword>();

            keywords.Add(new Keyword { Id = 1, Name = "Banana", CategoryId = 1 });
            keywords.Add(new Keyword { Id = 2, Name = "Apple", CategoryId = 1 });
            keywords.Add(new Keyword { Id = 3, Name = "Orange", CategoryId = 1 });
            keywords.Add(new Keyword { Id = 4, Name = "Basketball", CategoryId = 2 });
            keywords.Add(new Keyword { Id = 5, Name = "Golf", CategoryId = 2 });

            builder.HasData(keywords);

            builder.HasQueryFilter(e => !e.Deleted);
;        }
    }
}
