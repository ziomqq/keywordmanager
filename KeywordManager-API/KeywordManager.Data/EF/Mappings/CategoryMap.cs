﻿using KeywordManager.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace KeywordManager.Data.EF.Mappings
{
    public class CategoryMap : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(e => e.Id)
                .IsClustered();

            builder.Property(e => e.Created)
                .IsRequired()
                .HasColumnType("DateTime2")
                .HasDefaultValueSql("GETUTCDATE()")
                .HasConversion(v => v,
                    v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            builder
                .HasMany(c => c.Keywords)
                .WithOne(e => e.Category);

            List<Category> categories = new List<Category>();

            categories.Add(new Category { Id = 1, Name = "Fruits"});
            categories.Add(new Category { Id = 2, Name = "Sports" });

            builder.HasData(categories);

            builder.HasQueryFilter(e => !e.Deleted);
        }
    }
}
