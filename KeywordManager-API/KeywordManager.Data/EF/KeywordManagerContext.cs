﻿using KeywordManager.Core.Domain.Models;
using KeywordManager.Data.EF.Mappings;
using Microsoft.EntityFrameworkCore;

namespace KeywordManager.Data.EF
{
    public class KeywordManagerContext : DbContext
    {
        public KeywordManagerContext(DbContextOptions<KeywordManagerContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Keyword> Keywords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryMap());
            modelBuilder.ApplyConfiguration(new KeywordMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
