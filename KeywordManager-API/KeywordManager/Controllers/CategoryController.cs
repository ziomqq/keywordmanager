﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KeywordManager.Data.Repositories;
using KeywordManager.Models;
using Microsoft.AspNetCore.Mvc;
using CategoryEntity = KeywordManager.Core.Domain.Models.Category;

namespace KeywordManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;
        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public async Task<List<Category>> Get()
        {
            var categories = new List<Category>();
            var categoriesDb = await _categoryRepository.GetCategories();
            categoriesDb.ForEach(x => 
            {
                categories.Add(new Category(x));
            });

            return categories;
        }

        [HttpPost]
        public async Task<int> Post(CategoryEntity category)
        {
            return await _categoryRepository.AddOrUpdateCategory(category);
        }

        [HttpGet("[action]")]
        public bool Validate(string categoryName)
        {
            return _categoryRepository.ValidateName(categoryName);
        }

        [HttpDelete]
        public async Task<int> Delete(int categoryId)
        {
            return await _categoryRepository.DeleteCategory(categoryId);
        }
    }
}
