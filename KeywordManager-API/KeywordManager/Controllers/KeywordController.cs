﻿using System.Threading.Tasks;
using KeywordManager.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using KeywordEntity = KeywordManager.Core.Domain.Models.Keyword;

namespace KeywordManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KeywordController : ControllerBase
    {
        private readonly IKeywordRepository _keywordRepository;
        public KeywordController(IKeywordRepository keywordRepository)
        {
            _keywordRepository = keywordRepository;
        }

        [HttpPost]
        public async Task<int> Post(KeywordEntity keyword)
        {
            return await _keywordRepository.AddOrUpdateKeyword(keyword);
        }

        [HttpGet("[action]")]
        public bool Validate(int categoryId, string keywordName)
        {
            return _keywordRepository.ValidateName(categoryId, keywordName);
        }

        [HttpDelete]
        public async Task<int> Delete(int keywordId)
        {
            return await _keywordRepository.DeleteKeyword(keywordId);
        }
    }
}
