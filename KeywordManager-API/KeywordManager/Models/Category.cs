﻿using System.Collections.Generic;
using System.Linq;
using CategoryEntity = KeywordManager.Core.Domain.Models.Category;

namespace KeywordManager.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Keyword> Keywords { get; set; }

        public Category(CategoryEntity categoryDb)
        {
            this.Id = categoryDb.Id;
            this.Name = categoryDb.Name;
            this.Keywords = categoryDb.Keywords
                .Select(x => new Keyword(x)).ToList();
        }
    }
}
