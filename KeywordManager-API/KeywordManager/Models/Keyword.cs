﻿using KeywordEntity = KeywordManager.Core.Domain.Models.Keyword;

namespace KeywordManager.Models
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Keyword(KeywordEntity keywordDb)
        {
            this.Id = keywordDb.Id;
            this.Name = keywordDb.Name;
        }
    }
}
