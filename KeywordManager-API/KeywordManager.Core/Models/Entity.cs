﻿using System;

namespace KeywordManager.Core.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public bool Deleted { get; set; }
    }
}
