﻿using KeywordManager.Core.Models;

namespace KeywordManager.Core.Domain.Models
{
    public class Keyword : Entity
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
