﻿using KeywordManager.Core.Models;
using System.Collections.Generic;

namespace KeywordManager.Core.Domain.Models
{
    public class Category : Entity
    {
        public string Name { get; set; }

        public ICollection<Keyword> Keywords { get; set; }
    }
}
